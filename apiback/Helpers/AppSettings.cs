namespace VisionApi.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string ConnMySql { get; set; }
    }
}