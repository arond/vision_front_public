﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using VisionApi.Services;
using VisionApi.Entities;
using System;
using System.Linq;
using Microsoft.AspNetCore.Cors;

namespace WebApi.Controllers
{

    /*
    Les actions du contrôleur sont par défaut sécurisées avec JWT à l'aide de l'attribut [Authorize]
    sauf la method Authenticate qui est publique en redéfinissant [Authorize] du controller via l'attribut [AllowAnonymous]
    pour chaque route, le controller appelle le service user pour effectuer l'action requise afin qu'il reste lean et séparé de la business logic et du DAL
    */
    [EnableCors("_myAllowSpecificOrigins")]
    [Authorize]
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] User userParam)
        {
            User user;
            try
            {
                user = _userService.Authenticate(userParam.userName, userParam.password);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
           

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult AddUser([FromBody]User userParam)
        {
            try
            {
                int result = _userService.AddUser(userParam);
                if (result == 1 )
                    return Ok();
                else if (result == -1)
                    return BadRequest("User already exist");
                else
                    return BadRequest("Cannot insert user from server");
            }
            catch (Exception)
            {
                return BadRequest("Cannot insert user from server");
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var users = _userService.GetAll();
                users.ToList().ForEach(u => u.password = null);
                return Ok(users);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }  
        }

        [HttpGet("{id}")]
        public IActionResult GetAll(int id)
        {
            try
            {
                var users = _userService.GetAll();
                User user = users.ToList().Where(u => u.id == id).FirstOrDefault();
                if(user == null)
                    return NotFound("Unknow User");
                else
                    return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            try
            {
                int deleted = _userService.DeleteUserById(id);
                return Ok(deleted);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPatch("{id}/gpscoordinates")]
        public IActionResult UpdateGpsCoordinatesUser([FromBody]GpsCoordinates gpsCoordinates,int id)
        {
            try
            {
                int result = _userService.UpdateGpsCoordinates(id, gpsCoordinates);

                if (result == 1)
                    return Ok("ressource updated");
                if (result == -1)
                    return NotFound("Unknow User");
                else
                    return BadRequest("oups, something went wrong");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPatch("{id}/identity")]
        public IActionResult UpdateIdentityUser([FromBody]IdentityInfo identityInfo, int id)
        {
            try
            {
                int result = _userService.UpdateIdentityInfos(id, identityInfo);

                if (result == 1)
                    return Ok("ressource updated");
                if(result == -1)
                    return NotFound("Unknow User");
                else
                    return BadRequest("oups, something went wrong");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPatch("{id}/chrono")]
        public IActionResult UpdateChronoUser([FromBody]ChronoInfos chronoInfos, int id)
        {
            int result=-2;
            try
            {
                if(chronoInfos.typeChrono == "marche")
                    result = _userService.UpdateChronoMarche(id, chronoInfos);
                else if(chronoInfos.typeChrono == "course")
                    result = _userService.UpdateChronoCourse(id, chronoInfos);
                else if (chronoInfos.typeChrono == "velo")
                    result = _userService.UpdateChronoVelo(id, chronoInfos);


                if (result == 1)
                    return Ok("ressource updated");
                if (result == -1)
                    return NotFound("Unknow User");
                if (result == -2)
                    return NotFound("Unknow type of chrono");
                else
                    return BadRequest("oups, something went wrong");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
