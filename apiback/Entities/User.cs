namespace VisionApi.Entities
{
    public class User
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int lastUpdate { get; set; }
        public int chrono_marche { get; set; }
        public int chrono_course { get; set; }
        public int chrono_velo { get; set; }
        public string matricule { get; set; }
    }
}