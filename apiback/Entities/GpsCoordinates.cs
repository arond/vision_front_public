﻿namespace VisionApi.Entities
{
    public class GpsCoordinates
    {
        public string longitude { get; set; }
        public string latitude { get; set; }
    }
}
