using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Dapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using VisionApi.Entities;
using VisionApi.Helpers;

namespace VisionApi.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string pass);
        IEnumerable<User> GetAll();
        int DeleteUserById(int id);
        int AddUser(User userParam);
        int UpdateIdentityInfos(int id, IdentityInfo identityIfos);
        int UpdateGpsCoordinates(int id, GpsCoordinates gpsCoordinates);
        int UpdateChronoMarche(int id, ChronoInfos chronoInfos);
        int UpdateChronoVelo(int id, ChronoInfos chronoInfos);
        int UpdateChronoCourse(int id, ChronoInfos chronoInfos);
    }

    public class UserService : IUserService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private List<User> _users = new List<User>() ;
        //{ 
        //    new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" } 
        //};

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public User Authenticate(string username, string pass)
        {
            var users = GetAll();
            var user = users.SingleOrDefault(x => x.userName == username && x.password == pass);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.password = null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Query<User>(
                        @"SELECT id, firstName, lastName, userName, password, email, latitude, longitude, chrono_marche, chrono_course, chrono_velo, lastUpdate, matricule FROM User ").ToList();
                }
            }
            catch(Exception)
            {
                throw;
            }
           
        }

        public int DeleteUserById(int id)
        {
            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(@"DELETE FROM User WHERE id = @id ",
                        new { id });
                }
            }
            catch (MySqlException) { throw; }
        }

        public int AddUser(User userParam)
        {
            IEnumerable<User> users;
            try
            {
                users = GetAll();
            }
            catch (Exception)
            {
                throw;
            }
            

            if (users.FirstOrDefault(u => u.userName == userParam.userName) != null)
                return -1;

            string req = @"
INSERT INTO 
    User 
(
    firstName,
    lastName,
    userName,
    password,
    email,
    matricule
)
VALUES 
( 
    @firstName,
    @lastName,
    @userName,
    @password,
    @email,
    @matricule

)";
            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(req, new {
                        userParam.firstName,
                        userParam.lastName,
                        userParam.userName,
                        userParam.password,
                        userParam.email,
                        userParam.matricule}
                    );
                }
            }
            catch (MySqlException mySqlEx) { throw mySqlEx; }
            catch (Exception e) { throw e; }
        }

        public int UpdateIdentityInfos(int id, IdentityInfo identityInfos)
        {
            var users = GetAll();
            if (users.FirstOrDefault(u => u.id == id) == null)
                return -1;

            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(@"
UPDATE 
    User 
SET 
    firstName = @firstName ,
    lastName = @lastName ,
    email = @email
WHERE 
    id = @id ",
                            new { identityInfos.firstName, identityInfos.lastName, identityInfos.email, id });
                }
            }
            catch (MySqlException) { throw; }
        }

        public int UpdateGpsCoordinates(int id, GpsCoordinates gpsCoordinates)
        {
            var now = DateTime.Now;
            long currentUnixTime = ((DateTimeOffset)now).ToUnixTimeSeconds();

            var users = GetAll();
            if (users.FirstOrDefault(u => u.id == id) == null)
                return -1;

            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(@"
UPDATE 
    User 
SET 
    longitude = @longitude ,
    latitude = @latitude ,
    lastUpdate = @currentUnixTime
WHERE 
    id = @id ",
                            new { gpsCoordinates.longitude, gpsCoordinates.latitude, currentUnixTime,id });
                }
            }
            catch (MySqlException) { throw; }
        }

        public int UpdateChronoMarche(int id, ChronoInfos chronoInfos)
        {
            var now = DateTime.Now;
            long currentUnixTime = ((DateTimeOffset)now).ToUnixTimeSeconds();

            var users = GetAll();
            if (users.FirstOrDefault(u => u.id == id) == null)
                return -1;

            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(@"
UPDATE 
    User 
SET 
    chrono_marche = @chrono ,
    lastUpdate = @currentUnixTime
WHERE 
    id = @id ",
                            new { chronoInfos.chrono, currentUnixTime,id });
                }
            }
            catch (MySqlException) { throw; }
        }

        public int UpdateChronoCourse(int id, ChronoInfos chronoInfos)
        {
            var now = DateTime.Now;
            long currentUnixTime = ((DateTimeOffset)now).ToUnixTimeSeconds();

            var users = GetAll();
            if (users.FirstOrDefault(u => u.id == id) == null)
                return -1;

            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(@"
UPDATE 
    User 
SET 
    chrono_course = @chrono ,
    lastUpdate = @currentUnixTime
WHERE 
    id = @id ",
                            new { chronoInfos.chrono, currentUnixTime, id });
                }
            }
            catch (MySqlException e) { throw new Exception("mysql problem : "+e.Message); }
            catch (Exception e) { throw e; }
        }

        public int UpdateChronoVelo(int id, ChronoInfos chronoInfos)
        {
            var now = DateTime.Now;
            long currentUnixTime = ((DateTimeOffset)now).ToUnixTimeSeconds();

            var users = GetAll();
            if (users.FirstOrDefault(u => u.id == id) == null)
                return -1;

            try
            {
                using (MySqlConnection db = new MySqlConnection(_appSettings.ConnMySql))
                {
                    return db.Execute(@"
UPDATE 
    User 
SET 
    chrono_velo = @chrono ,
    lastUpdate = @currentUnixTime
WHERE 
    id = @id ",
                            new { chronoInfos.chrono, currentUnixTime, id});
                }
            }
            catch (MySqlException) { throw; }
        }
    }
}