﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

/*
authentication.service va servir pour se login/logout à l'appli
pour se logger => request POST avec user credentials à l'api et check si la response contient le JWT token
s'il y a un token c'est que l'authentification a reussi donc les infos du user sont ajouté au local storage avec le token
le token est utilisé par JWT pour setter le authorization header des requetes http pour secure les echange avec les routes de l'api.

Les détails du user connecté sont stockés dans le local storage 
donc le user reste connecté s'il refresh son navigateur et entre les differentes sessions du browser jusqua sa deconnexion. 

Si on ne veux pas que le user reste connecté entre ses actualisations et sessions :
son comportement peut etre modifié en stockant les details du user dans un endroit moins persistant tel que le sessionStorage qui persisterait entre les refresh
du browser mais pas entre les sessions, 
ou dans une variable private dans le service authentication qui serait clear quand il y a une actualisation du navigateur

authentication service presente 2 properties permettant d'acceder au user logged, l'observable currentUser peut etre utilisé si on veut qu'un component 
se mette a jour de maniere reactive lorqu'un user se login ou se logout.
exemple dans le fichier app.component.ts afin qu'il puisse afficher/masquer la barre de navigation  lorsque le user se login/logout,
la propriété currentUserValue peut etre utilisé quand on veut obtenir la value actuelle du user logged mais quon a pas besoin de MAJ de maniere reactive lorsquelle
change par exemple dans auth.guard.ts, qui restreint l'accès aux itinéraires en vérifiant si le user est actuellement logged.
*/

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // login successful si le jwt token est dans la réponse
                if (user && user.token) {
                    // stockage des user details et du jwt token dans le local storage pour maintenir le user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // supprimer le user du local storage pour le logout
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}