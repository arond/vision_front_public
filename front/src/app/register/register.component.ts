﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MustMatch } from '../_helpers/must-match.validator';
import { Observable } from 'rxjs';
import { AlertService, UserService, AuthenticationService } from '@app/_services';

@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    titleAlert: string = 'Champ requis';
    post: any = '';

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService
    ) { 
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {

        let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            userName: ['', Validators.required],
            'email': ['', [Validators.required, Validators.pattern(emailregex)]],
            password: ['', [Validators.required, this.checkPassword]],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit(post: any) {
        this.submitted = true;
        this.post = post;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Inscription effectuée !', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

    checkPassword(control: { value: any; }) {
        let enteredPassword = control.value
        let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
        return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
      }

      getErrorPassword() {
        return this.registerForm.get('password').hasError('required') ? 'Mot de passe requis (au minimum 8 caractères, 1 majuscule et 1 chiffre)' :
          this.registerForm.get('password').hasError('requirements') ? 'Le mot de passe doit contenir au minimum 8 caractères, 1 majuscule et 1 chiffre' : '';
      }

      checkInUseEmail(control: { value: string; }) {
        // mimic http database access
        let db = ['tony@gmail.com'];
        return new Observable(observer => {
          setTimeout(() => {
            let result = (db.indexOf(control.value) !== -1) ? { 'alreadyInUse': true } : null;
            observer.next(result);
            observer.complete();
          }, 4000)
        })
      }
    
      getErrorEmail() {
        return this.registerForm.get('email').hasError('required') ? 'Email requis' :
          this.registerForm.get('email').hasError('pattern') ? 'Email invalide' :
            this.registerForm.get('email').hasError('alreadyInUse') ? 'Cet addresse email est déja utilisée' : '';
      }
}
