package fr.taupe.vision;

public class Singleton {
    private static final Singleton ourInstance = new Singleton();
    public static String status = "";
    public static String token = "";
    public static int id = 0;
    public static  String[] android_cest_nul = new String[3]; // LD 2019
    static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
    }
}
