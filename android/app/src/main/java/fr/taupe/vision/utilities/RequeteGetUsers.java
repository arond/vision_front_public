package fr.taupe.vision.utilities;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import fr.taupe.vision.Singleton;

public class RequeteGetUsers {
    public static void call_user(Context context)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Bearer " + Singleton.token);
        client.get(context,"http://163.172.180.87:60281/api/users", new RequestParams(),new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                Log.d("onstart","start");
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.d("onsuccess","success");
                Log.e("success response ", new String(response));
                //String str = new String(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.e("failure http", new String(errorResponse));
                Log.d("failure", "fail");
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
