package fr.taupe.vision.utilities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;
import fr.taupe.vision.Singleton;
import fr.taupe.vision.utilities.db.GPSReaderContract;
import fr.taupe.vision.utilities.db.GPSReaderDbHelper;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GPSTracker {
    private double longitude;
    private double latitude;
    public static double longi;
    public static double lat;
    Location lastLocation = null;
    LocationManager lm;
    Context mContext;
    public static float distance = 0;
    boolean enabled;
    public static String url = "http://163.172.180.87:60281/api/users/"+Singleton.id+"/gpscoordinates";

    public GPSTracker(Context c) {
        mContext = c;
        longitude = 0;
        latitude = 0;
        lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
    }

    public boolean isEnabled() {
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    LocationListener ll = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Toast.makeText(mContext, location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_LONG).show();
            if(lastLocation ==null)
            {
                lastLocation = location;
            }
            else
            {
                //float[] floats = new float[4];
                //Location.distanceBetween(lastLocation.getLongitude(), lastLocation.getLatitude(), location.getLongitude(), location.getLatitude(), floats);
                distance = distance + lastLocation.distanceTo(location);
                lastLocation = location;
            }
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            GPSTracker.longi = longitude;
            GPSTracker.lat = latitude;
            GPSReaderDbHelper dbHelper = new GPSReaderDbHelper(mContext);
            // Gets the data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(GPSReaderContract.GPSEntry.COLUMN_NAME_LONGI, longitude);
            values.put(GPSReaderContract.GPSEntry.COLUMN_NAME_LATI, latitude);

            // Insert the new row, returning the primary key value of the new row
            long newRowId = db.insert(GPSReaderContract.GPSEntry.TABLE_NAME, null, values);

            new Task_patch().execute();
          //  RequeteGPS.request(mContext, Double.toString(longitude), Double.toString(latitude), Singleton.id);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Toast.makeText(mContext, "status : " + status, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(mContext, "enabled", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void getData(int minTime, int minRange) {
        if (ActivityCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minRange, ll);

        LocationListener locationListener = new MyLocationListener();
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
    }


    public void isLocationEnabled() {

        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            alertDialog.setTitle("Enable Location");
            alertDialog.setMessage("Your locations setting is not enabled. Please enabled it in settings menu.");
            alertDialog.setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }

     //   Toast.makeText(mContext,"islocation enabled",Toast.LENGTH_LONG).show();

    }


    class Task_patch extends AsyncTask<String, Void, Boolean> {
        protected Boolean doInBackground(String... url) {

            OkHttpClient client = new OkHttpClient();
            MediaType JSON
                    = MediaType.get("application/json; charset=utf-8");

            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("chrono", Chrono.value);
                jsonParams.put("typeChrono", "marche");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("d","5");
            StringEntity entity = null;
            try {
                entity = new StringEntity(jsonParams.toString());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(JSON, "{ \"chrono\": 330, \"typeChrono\": \"marche\"}");

            body = RequestBody.create("{" +
                    "  \"longitude\": \""+GPSTracker.longi+"\"," +
                    "  \"latitude\": \""+GPSTracker.lat+"\"" +
                    "}", JSON);
            Log.e("d","6");
            Request request = new Request.Builder()
                    .header("Authorization", "Bearer "+Singleton.token)
                    .url(GPSTracker.url)
                    .patch(body)
                    .build();
            String resp = null;
            try {
                Response response = client.newCall(request).execute();
                resp = response.body().string();
                Log.e("log", response.headers().value(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("patch", resp);
            return true;
        }
    }
}
