package fr.taupe.vision;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Chronometer;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import fr.taupe.vision.utilities.Chrono;
import fr.taupe.vision.utilities.GPSTracker;
import fr.taupe.vision.utilities.RequeteAuthentificate;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChronoActivity extends AppCompatActivity {
    GPSTracker gps;
    Chrono chrono;
    int limite;
    String notif;
    String desc;
    private Chronometer c;
    private Button start;
    private Button pause;
    private Button reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrono);

        Intent intent = getIntent();

        //gps
        gps = new GPSTracker(ChronoActivity.this);
        gps.isLocationEnabled();
        gps.getData(intent.getIntExtra("minTime",5000),intent.getIntExtra("minRange",5));
        limite = intent.getIntExtra("limite",30);
        notif = intent.getStringExtra("notif");
        desc = intent.getStringExtra("desc");

        //instanciation des widgets
        c = findViewById(R.id.chrono);
        start = findViewById(R.id.start);
        pause = findViewById(R.id.pause);
        reset = findViewById(R.id.reset);

        chrono = new Chrono(limite,c,start,pause,reset,getBaseContext(),notif,desc);
                chrono.button_listener(chrono.getStart());
                chrono.button_listener(chrono.getPause());
                chrono.button_listener(chrono.getReset());
                chrono.chrono_listener(chrono.getchrono());

              //  RequeteAuthentificate.request(getBaseContext());
                }




                }
