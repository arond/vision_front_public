package fr.taupe.vision.utilities.db;

import android.provider.BaseColumns;

public final class GPSReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private GPSReaderContract() {}

    /* Inner class that defines the table contents */
    public static class GPSEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_LONGI = "longitude";
        public static final String COLUMN_NAME_LATI= "altitude";
    }
}
