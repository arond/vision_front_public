package fr.taupe.vision.utilities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

import cz.msebera.android.httpclient.entity.StringEntity;
import fr.taupe.vision.MainActivity;
import fr.taupe.vision.R;
import fr.taupe.vision.Singleton;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Chrono extends AppCompatActivity {
    private int limite;
    private Chronometer c;
    private Button start;
    private Button pause;
    private Button reset;
    private long current;
    boolean run;
    final String CHANNEL_ID = "Marche";
    final int NOTIFICATION_ID = 1;
    long[] vibration = new long[]{0, 200, 100, 200, 100, 200};
    private Context context;
    private String titre;
    private static String description;
    public static String url = "http://163.172.180.87:60281/api/users/"+Singleton.id+"/chrono";
    public static long value = 0;

    public Chrono(int limite_chrono,Chronometer c,Button start,Button pause,Button reset,Context context,String titre,String description){
        this.limite = limite_chrono;
        this.c = c;
        this.start = start;
        this.pause = pause;
        this.reset = reset;
        this.context = context;
        getchrono().setBase(SystemClock.elapsedRealtime());
        this.titre = titre;
        Chrono.description = description;
    }

    public void button_listener(Button b){
        String choice = b.getText().toString();
        switch(choice){
            case "Start":
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!run) {
                            getchrono().setBase(SystemClock.elapsedRealtime() - current);
                            getchrono().start();
                            run = true;
                        }
                    }
                });
                break;
            case "Pause":
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (run) {
                            getchrono().stop();
                            current = SystemClock.elapsedRealtime() - getchrono().getBase();
                            run = false;
                            long notif_listener = SystemClock.elapsedRealtime() - getchrono().getBase();
                            notif_listener = notif_listener/1000;
                            Chrono.value = notif_listener;
                        }
                        new Task_patch().execute();
                    }
                });
                break;
            case "Reset":
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getchrono().setBase(SystemClock.elapsedRealtime());
                        current = 0;
                    }
                });
        }
    }

    public void chrono_listener(final Chronometer c){
        c.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long notif_listener = SystemClock.elapsedRealtime() - chronometer.getBase();
                notif_listener = notif_listener/1000;
                if(notif_listener == limite){
                    // Créer le NotificationChannel, seulement pour API 26+
                    //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Log.e("debug notif","avant");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        Log.e("debug notif","j'enttre'");
                        CharSequence name = "Notification channel name";
                        int importance = NotificationManager.IMPORTANCE_DEFAULT;
                        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
                        channel.setDescription("Notification channel description");
                        // Enregister le canal sur le système : attention de ne plus rien modifier après
                        // NotificationManager notificationManager = getSystemService(NotificationManager.class);
                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
                       // Log.e("chrono", "bouton pause");
                       // RequeteChrono.request(context,420,"marche", Singleton.id);
                    }
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                    NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_local_hospital)
                            .setContentTitle(titre)
                            .setContentText(description)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .setVibrate(vibration);

                    // notificationId est un identificateur unique par notification qu'il vous faut définir
                    notificationManager.notify(NOTIFICATION_ID, notifBuilder.build());
                    new Task_patch().execute();
                }
            }
        });
    }


    public Button getStart() {
        return start;
    }

    public Button getPause() {
        return pause;
    }

    public Button getReset() {
        return reset;
    }

    public Chronometer getchrono() {
        return c;
    }

    class Task_patch extends AsyncTask<String, Void, Boolean> {
        protected Boolean doInBackground(String... url) {

            OkHttpClient client = new OkHttpClient();
            MediaType JSON
                    = MediaType.get("application/json; charset=utf-8");

            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("chrono", Chrono.value);
                jsonParams.put("typeChrono", "marche");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("d","5");
            StringEntity entity = null;
            try {
                entity = new StringEntity(jsonParams.toString());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(JSON, "{ \"chrono\": 330, \"typeChrono\": \"marche\"}");
            String type = "marche";
            if(Chrono.description.equals("Vous avez couru 30 minutes"))
            {
                 type = "course";
            }
            else if(Chrono.description.equals("Vous avez fait 30 minutes de vélo"))
            {
                type = "velo";
            }
            else
            {
                 type = "marche";
            }
            body = RequestBody.create("{ \"chrono\": "+Chrono.value+", \"typeChrono\": \""+type+"\"}", JSON);
            Log.e("d","6");
            Request request = new Request.Builder()
                    .header("Authorization", "Bearer "+Singleton.token)
                    .url(Chrono.url)
                    .patch(body)
                    .build();
            String resp = null;
             try {
               Response response = client.newCall(request).execute();
                 resp = response.body().string();
                 Log.e("log", response.headers().value(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
                Log.e("patch", resp);
            return true;
        }
    }
}