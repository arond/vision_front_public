package fr.taupe.vision.utilities.db;

public class GPS_info {
    public String longitude = "";
    public String latitude = "";
    public Long id;

    public GPS_info(Long id, String l, String ll)
    {
        this.id = id;
        this.longitude = l;
        this.latitude = ll;
    }
}
