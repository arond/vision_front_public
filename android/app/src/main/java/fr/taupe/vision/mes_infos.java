package fr.taupe.vision;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import fr.taupe.vision.utilities.GPSTracker;
import fr.taupe.vision.utilities.db.GPSReaderContract;
import fr.taupe.vision.utilities.db.GPSReaderDbHelper;
import fr.taupe.vision.utilities.db.GPS_info;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class mes_infos extends AppCompatActivity {
    public static String url = "http://163.172.180.87:60281/api/users/"+Singleton.id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_infos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView longitude = findViewById(R.id.longitude);
        TextView latitude = findViewById(R.id.latitude);
        TextView username = findViewById(R.id.username);
        TextView nom = findViewById(R.id.nom);
        TextView prenom = findViewById(R.id.prenom);
        TextView email = findViewById(R.id.email);
        TextView cc = findViewById(R.id.chrono_course);
        TextView cm = findViewById(R.id.chrono_marche);
        TextView cv = findViewById(R.id.chrono_velo);
        TextView dist = findViewById(R.id.distance);
        String datas = "";
        try {
            datas = new Task_get().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String[] t = new String[9];
        String token = "";
        int id = 0;
        try {
            JSONObject testV=new JSONObject(new String(datas));
            prenom.setText("nom :"+testV.getString("firstName"));
            nom.setText("prenom :"+testV.getString("lastName"));
            username.setText("login :"+testV.getString("userName"));
            email.setText("email :"+testV.getString("email"));
            latitude.setText("latitude :"+testV.getString("latitude"));
            longitude.setText("longitude :"+testV.getString("longitude"));
            cc.setText("Secondes de course :" + testV.getString("chrono_course"));
            cm.setText("Secondes de marche :"+testV.getString("chrono_marche"));
            cv.setText("Secondes de velo :"+testV.getString("chrono_velo"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dist.setText("distance approximative parcourue : " + GPSTracker.distance);
    }

    public List<GPS_info> list()
    {
        GPSReaderDbHelper dbHelper = new GPSReaderDbHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                BaseColumns._ID,
                GPSReaderContract.GPSEntry.COLUMN_NAME_LONGI,
                GPSReaderContract.GPSEntry.COLUMN_NAME_LATI
        };

// Filter results WHERE "title" = 'My Title'
        String selection = GPSReaderContract.GPSEntry.COLUMN_NAME_LONGI + " = ?";
        String[] selectionArgs = { "My Title" };

// How you want the results sorted in the resulting Cursor
        String sortOrder =
                GPSReaderContract.GPSEntry.COLUMN_NAME_LATI+ " DESC";

        Cursor cursor = db.query(
                GPSReaderContract.GPSEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        List items = new ArrayList<>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(GPSReaderContract.GPSEntry._ID));
            String longi = cursor.getString(
                    cursor.getColumnIndexOrThrow(GPSReaderContract.GPSEntry.COLUMN_NAME_LONGI));
            String lati = cursor.getString(
                    cursor.getColumnIndexOrThrow(GPSReaderContract.GPSEntry.COLUMN_NAME_LATI));
            //items.add(itemId);
            items.add(new GPS_info(itemId, longi, lati));
        }
        cursor.close();
        return items;
    }

    class Task_get extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... url) {

            OkHttpClient client = new OkHttpClient();
            MediaType JSON
                    = MediaType.get("application/json; charset=utf-8");

            Log.e("d","6");
            Request request = new Request.Builder()
                    .header("Authorization", "Bearer "+Singleton.token)
                    .url(mes_infos.url)
                    .get()
                    .build();
            String resp = null;
            try {
                Response response = client.newCall(request).execute();
                resp = response.body().string();
                Log.e("log", response.headers().value(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("patch", resp);
            return resp;
        }
    }
}
