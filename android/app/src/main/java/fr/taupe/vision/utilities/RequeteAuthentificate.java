package fr.taupe.vision.utilities;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import fr.taupe.vision.Singleton;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequeteAuthentificate {
    public static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");

    public OkHttpClient client = new OkHttpClient();
    public static void request(final Context context)
    {
        RequestParams rp = new RequestParams();
        rp.add("username", "aurel"); rp.add("password", "aurel");
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("userName", "aurel");
            jsonParams.put("password", "aurel");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonParams.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(context,"http://163.172.180.87:60281/api/users/authenticate", entity,"application/json" ,new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                Log.d("onstart","start");
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.d("onsuccess","success");
                Log.e("success response ", new String(response));
                //String str = new String(response);
                String token = "";
                int id = 0;
                try {
                    JSONObject testV=new JSONObject(new String(response));
                    token = testV.getString("token");
                    id = testV.getInt("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(token != "")
                    Singleton.token = token;
                    Singleton.id = id;
                // called when response HTTP status is "200 OK"

                RequeteGetUsers.call_user(context);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d("failure", "fail");
                Log.e("failure http", new String(errorResponse));

            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

        //////////////
    }

}
