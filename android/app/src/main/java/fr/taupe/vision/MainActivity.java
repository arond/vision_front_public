package fr.taupe.vision;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.hash.Hashing;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import fr.taupe.vision.utilities.Requete;
import fr.taupe.vision.utilities.RequeteAuthentificate;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static fr.taupe.vision.utilities.RequeteAuthentificate.JSON;

public class MainActivity extends AppCompatActivity {
    public static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");

    public OkHttpClient client = new OkHttpClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bweb = findViewById(R.id.web);

        bweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String requete = getResources().getString(R.string.compte);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requete));
                startActivity(intent);
            }
        });

        Button l = findViewById(R.id.blogin);
        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText login = findViewById(R.id.login_value);
                EditText pass = findViewById(R.id.password_value);
                String[] t = new String[3];
                t[0] = "http://163.172.180.87:60281/api/users/authenticate";
                t[1] = login.getText().toString();
                t[2] = pass.getText().toString();
                Log.e("d","1");
                Singleton.android_cest_nul = t;
                Boolean b = false;
                Log.e("d","2");
                try {
                     b = new Task().execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.e("d","3");
                if(b)
                {
                    Intent intent = new Intent(v.getContext(),ChoiceActivity.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getBaseContext(), "Authentification failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    class Task extends AsyncTask<String, Void, Boolean> {
        protected Boolean doInBackground(String... url) {
            JSONObject jsonParams = new JSONObject();
            try {
                Log.e("d","4");
        jsonParams.put("userName", Singleton.android_cest_nul[1]);
                String sha256hex = Hashing.sha256()
                        .hashString(Singleton.android_cest_nul[2], StandardCharsets.UTF_8)
                        .toString();
                Log.e(sha256hex, Singleton.android_cest_nul[2]);
        jsonParams.put("password", sha256hex);
    } catch (JSONException e) {
        e.printStackTrace();
    }
            Log.e("d","5");
    StringEntity entity = null;
            try {
        entity = new StringEntity(jsonParams.toString());
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
            Log.e("d","6");
    RequestBody body = RequestBody.create(JSON, jsonParams.toString());
    Request request = new Request.Builder()
            .url(Singleton.android_cest_nul[0])
            .post(body)
            .build();
            Log.e("d","7");
            String token = "";
            int id = 0;
            try (Response response = client.newCall(request).execute()) {
                Log.e("d","8");
        String resp = null;
        try {
            resp = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("okhhtp authen", resp);

        try {
            JSONObject testV=new JSONObject(new String(resp));
            token = testV.getString("token");
            id = testV.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(token != "")
            {
                Singleton.token = token;
                Singleton.id = id;
                Log.e("auth","Authentification Reussie");
                return true;
            }
            else
            {
                Log.e("auth","Authentification Echouee");
                return false;
            }
}
    }
}