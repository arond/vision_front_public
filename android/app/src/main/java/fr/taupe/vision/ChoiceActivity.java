package fr.taupe.vision;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);

        //Bouton de marche
        Button marche = findViewById(R.id.marche) ;
        marche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),ChronoActivity.class);
                intent.putExtra("limite",15);//temps de marche
                intent.putExtra("minTime",2000);//pool du gps en ms pour update
                intent.putExtra("minRange",2);//distance mini en m pour update
                intent.putExtra("notif",getResources().getString(R.string.marche_notif));
                intent.putExtra("desc",getResources().getString(R.string.marche_desc));
                startActivity(intent);
            }
        });

        //Bouton de course
        Button course = findViewById(R.id.course);
        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),ChronoActivity.class);
                intent.putExtra("limite",10);//temps de marche
                intent.putExtra("minTime",5000);//pool du gps en ms pour update
                intent.putExtra("minRange",3);//distance mini en m pour update
                intent.putExtra("notif",getResources().getString(R.string.course_notif));
                intent.putExtra("desc",getResources().getString(R.string.course_desc));
                startActivity(intent);
            }
        });

        Button velo = findViewById(R.id.velo);
        velo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),ChronoActivity.class);
                intent.putExtra("limite",5);//temps de marche
                intent.putExtra("minTime",10000);//pool du gps en ms pour update
                intent.putExtra("minRange",5);//distance mini en m pour update
                intent.putExtra("notif",getResources().getString(R.string.velo_notif));
                intent.putExtra("desc",getResources().getString(R.string.velo_desc));
                startActivity(intent);
            }
        });

        Button infos = findViewById(R.id.infos);
        infos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),mes_infos.class);
                startActivity(intent);
            }
        });
    }
}
