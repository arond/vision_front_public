package fr.taupe.vision.utilities;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import fr.taupe.vision.Singleton;

public class RequeteGPS {
    public static void request(final Context context, String longitude, String latitude, int user_id)
    {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("longitude", longitude);
            jsonParams.put("latitude", latitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonParams.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Bearer " + Singleton.token);
        client.patch(context,"http://163.172.180.87:60281/api/users/"+user_id+"/chrono", entity,"application/json" ,new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                Log.d("onstart","start");
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.d("onsuccess","success");
                Log.e("success Chrono Patch", new String(response));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.e("failure http", new String(errorResponse));
                Log.d("failure", "fail");
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

        //////////////
    }
}
